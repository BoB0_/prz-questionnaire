package com.prz.mapper;

import com.prz.dto.EmployeeCreateDTO;
import com.prz.dto.EmployeeViewDTO;
import com.prz.model.Employee;

/**
 * Created by rbobko on 20.12.2016.
 */
public class EmployeeMapper {

    public static EmployeeViewDTO loadFromEntity(Employee employee){
        EmployeeViewDTO employeeDTO = new EmployeeViewDTO();

        employeeDTO.setId(employee.getId());
        employeeDTO.setLogin(employee.getLogin());
        employeeDTO.setEmail(employee.getEmail());
        employeeDTO.setFirstName(employee.getFirstName());
        employeeDTO.setLastName(employee.getLastName());
        employeeDTO.setDepartmentId(employee.getDepartmentId());
        employeeDTO.setCathedralId(employee.getCathedralId());
        employeeDTO.setRole(employee.getRole().toString());

        return employeeDTO;
    }

    public static Employee mapToEntity(EmployeeCreateDTO employeeDTO){
        Employee employee = new Employee();

        employee.setLogin(employeeDTO.getLogin());
        employee.setPassword(employeeDTO.getPassword());
        employee.setEmail(employeeDTO.getEmail());
        employee.setFirstName(employeeDTO.getFirstName());
        employee.setLastName(employeeDTO.getLastName());

        return employee;
    }
}

package com.prz.mapper;

import com.prz.dto.TextAnswerDTO;
import com.prz.model.TextAnswer;
import com.prz.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbobko on 20.12.2016.
 */
@Component
public class TextAnswerMapper {

    @Autowired
    QuestionService questionService;

    public TextAnswer mapToEntity(TextAnswerDTO textAnswerDTO){
        TextAnswer textAnswer = new TextAnswer();

        textAnswer.setQuestion(questionService.findOne(textAnswerDTO.getQuestionId()));
        textAnswer.setText(textAnswerDTO.getText());

        return textAnswer;
    }

    public List<TextAnswer> mapToEntity(List<TextAnswerDTO> textAnswerDTOs){
        List<TextAnswer> textAnswers = new ArrayList<>();

        textAnswerDTOs.forEach(textAnswer -> textAnswers.add(mapToEntity(textAnswer)));

        return textAnswers;
    }

    public TextAnswerDTO loadFromEntity(TextAnswer textAnswer){
        TextAnswerDTO textAnswerDTO = new TextAnswerDTO();

        textAnswerDTO.setId(textAnswer.getId());
        textAnswerDTO.setQuestionId(textAnswer.getQuestion().getId());
        textAnswerDTO.setText(textAnswer.getText());

        return textAnswerDTO;
    }

    public List<TextAnswerDTO> loadFromEntity(List<TextAnswer> textAnswers){
        List<TextAnswerDTO> textAnswerDTOS = new ArrayList<>();

        textAnswers.forEach(textAnswer -> textAnswerDTOS.add(loadFromEntity(textAnswer)));

        return textAnswerDTOS;
    }
}

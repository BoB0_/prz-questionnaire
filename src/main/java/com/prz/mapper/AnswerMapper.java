package com.prz.mapper;

import com.prz.dto.AnswerDTO;
import com.prz.model.Answer;
import com.prz.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@Component
public class AnswerMapper {

    @Autowired
    private QuestionService questionService;

    public Answer mapToEntity(AnswerDTO answerDTO){
        Answer answer = new Answer();

        answer.setText(answerDTO.getText());
        if(answerDTO.getQuestionId()!=null) {
            answer.setQuestion(questionService.findOne(answerDTO.getQuestionId()));
        }

        return answer;
    }

    public List<Answer> mapToEntity(List<AnswerDTO> answerDTOs){
        List<Answer> answers = new ArrayList<>();

        answerDTOs.forEach(answerDTO -> answers.add(mapToEntity(answerDTO)));

        return answers;
    }

    public AnswerDTO loadFromEntity(Answer answer){
        AnswerDTO answerDTO = new AnswerDTO();

        answerDTO.setText(answer.getText());
        answerDTO.setId(answer.getId());
        answerDTO.setQuestionId(answer.getQuestion().getId());

        return answerDTO;
    }

    public List<AnswerDTO> loadFromEntity(List<Answer> answers){
        List<AnswerDTO> answerDTOs = new ArrayList<>();

        answers.forEach(answer -> answerDTOs.add(loadFromEntity(answer)));

        return answerDTOs;
    }
}

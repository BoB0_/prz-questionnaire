package com.prz.mapper;

import com.prz.dto.SolvedQuestionnaireCreateDTO;
import com.prz.dto.SolvedQuestionnaireViewDTO;
import com.prz.model.Answer;
import com.prz.model.Employee;
import com.prz.model.SolvedQuestionnaire;
import com.prz.service.AnswerService;
import com.prz.service.EmployeeService;
import com.prz.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbobko on 20.12.2016.
 */
@Component
public class SolvedQuestionnaireMapper {

    @Autowired
    private AnswerMapper answerMapper;

    @Autowired
    private TextAnswerMapper textAnswerMapper;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private QuestionnaireMapper questionnaireMapper;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private EmployeeService employeeService;

    public SolvedQuestionnaire mapToEntity(SolvedQuestionnaireCreateDTO solvedQuestionnaireDTO) {
        SolvedQuestionnaire solvedQuestionnaire = new SolvedQuestionnaire();

        Employee loggedUser = employeeService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        solvedQuestionnaire.setEmployeeId(loggedUser.getId());
        solvedQuestionnaire.setQuestionnaireId(solvedQuestionnaireDTO.getQuestionnaireId());
        solvedQuestionnaire.setSolvingDate(LocalDateTime.now());

        if(solvedQuestionnaireDTO.getAnswersIds() != null) {
            List<Answer> answers = new ArrayList<>();
            solvedQuestionnaireDTO.getAnswersIds().forEach(id -> answers.add(answerService.findOne(id)));
            solvedQuestionnaire.setAnswers(answers);
        }
        if(solvedQuestionnaireDTO.getTextAnswers() != null) {
            solvedQuestionnaire.setTextAnswers(textAnswerMapper.mapToEntity(solvedQuestionnaireDTO.getTextAnswers()));
        }

        return solvedQuestionnaire;
    }

    public SolvedQuestionnaireViewDTO loadFromEntity(SolvedQuestionnaire solvedQuestionnaire) {
        SolvedQuestionnaireViewDTO solvedQuestionnaireViewDTO = new SolvedQuestionnaireViewDTO();

        solvedQuestionnaireViewDTO.setId(solvedQuestionnaire.getId());
        if(solvedQuestionnaire.getEmployeeId() != null) {
            Employee employee = employeeService.findOne(solvedQuestionnaire.getEmployeeId());
            solvedQuestionnaireViewDTO.setEmployee(EmployeeMapper.loadFromEntity(employee));
        }
        solvedQuestionnaireViewDTO.setQuestionnaire(questionnaireMapper.loadFromEntity(questionnaireService.findOne(solvedQuestionnaire.getQuestionnaireId())));
        solvedQuestionnaireViewDTO.setSolvingDate(solvedQuestionnaire.getSolvingDate());
        solvedQuestionnaireViewDTO.setAnswers(answerMapper.loadFromEntity(solvedQuestionnaire.getAnswers()));
        solvedQuestionnaireViewDTO.setTextAnswers(textAnswerMapper.loadFromEntity(solvedQuestionnaire.getTextAnswers()));

        return solvedQuestionnaireViewDTO;
    }
}

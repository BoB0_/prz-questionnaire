package com.prz.mapper;

import com.prz.dto.QuestionDTO;
import com.prz.model.Question;
import com.prz.model.QuestionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@Component
public class QuestionMapper {

    @Autowired
    private AnswerMapper answerMapper;

    public Question mapToEntity(QuestionDTO questionDTO){
        Question question = new Question();

        question.setText(questionDTO.getText());
        question.setType(questionDTO.getType());
        if(!questionDTO.getType().equals(QuestionType.TEXT)) {
            question.setAnswers(answerMapper.mapToEntity(questionDTO.getAnswers()));
            question.getAnswers().forEach(answer -> answer.setQuestion(question));
        }

        return question;
    }

    public List<Question> mapToEntity(List<QuestionDTO> questionDTOs){
        List<Question> questions = new ArrayList<>();

        questionDTOs.forEach(questionDTO -> questions.add(mapToEntity(questionDTO)));

        return questions;
    }

    public QuestionDTO loadFromEntity(Question question){
        QuestionDTO questionDTO = new QuestionDTO();

        questionDTO.setId(question.getId());
        questionDTO.setText(question.getText());
        questionDTO.setType(question.getType());
        if(!question.getType().equals(QuestionType.TEXT)) {
            questionDTO.setAnswers(answerMapper.loadFromEntity(question.getAnswers()));
        }

        return questionDTO;
    }

    public List<QuestionDTO> loadFromEntity(List<Question> questions){
        List<QuestionDTO> questionDTOs = new ArrayList<>();

        questions.forEach(question -> questionDTOs.add(loadFromEntity(question)));

        return questionDTOs;
    }
}

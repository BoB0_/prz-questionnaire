package com.prz.mapper;

import com.prz.dto.QuestionnaireDTO;
import com.prz.model.Questionnaire;
import com.prz.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@Component
public class QuestionnaireMapper {

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private EmployeeService employeeService;

    public Questionnaire mapToEntity(QuestionnaireDTO questionnaireDTO) {
        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setCreationDate(LocalDateTime.now());
        if(questionnaireDTO.getCreator()!=null) {
            questionnaire.setCreatorId(employeeService.findByLogin(questionnaireDTO.getCreator()).getId());
        }
        questionnaire.setDescription(questionnaireDTO.getDescription());
        questionnaire.setName(questionnaireDTO.getName());
        questionnaire.setDepartments(questionnaireDTO.getDepartments());
        questionnaire.setCathedrals(questionnaireDTO.getCathedrals());
        questionnaire.setQuestions(questionMapper.mapToEntity(questionnaireDTO.getQuestions()));

        return questionnaire;
    }

    public QuestionnaireDTO loadFromEntity(Questionnaire questionnaire) {
        QuestionnaireDTO questionnaireDTO = new QuestionnaireDTO();

        questionnaireDTO.setId(questionnaire.getId());
        questionnaireDTO.setName(questionnaire.getName());
        questionnaireDTO.setCreationDate(questionnaire.getCreationDate());
        if(questionnaire.getCreatorId()!=null) {
            questionnaireDTO.setCreator(employeeService.findOne(questionnaire.getCreatorId()).getLogin());
        }
        questionnaireDTO.setDescription(questionnaire.getDescription());
        questionnaireDTO.setDepartments(questionnaire.getDepartments());
        questionnaireDTO.setCathedrals(questionnaire.getCathedrals());
        questionnaireDTO.setQuestions(questionMapper.loadFromEntity(questionnaire.getQuestions()));

        return questionnaireDTO;
    }

    public List<QuestionnaireDTO> loadFromEntity(List<Questionnaire> questionnaires) {
        List<QuestionnaireDTO> questionnaireDTOs = new ArrayList<>();

        questionnaires.forEach(questionnaire -> questionnaireDTOs.add(loadFromEntity(questionnaire)));

        return questionnaireDTOs;
    }
}

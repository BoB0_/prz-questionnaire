package com.prz.dto;

import java.util.List;

/**
 * Created by rbobko on 20.12.2016.
 */
public class SolvedQuestionnaireCreateDTO {

    private Long questionnaireId;

    private List<Long> answersIds;

    private List<TextAnswerDTO> textAnswers;

    public Long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public List<Long> getAnswersIds() {
        return answersIds;
    }

    public void setAnswersIds(List<Long> answersIds) {
        this.answersIds = answersIds;
    }

    public List<TextAnswerDTO> getTextAnswers() {
        return textAnswers;
    }

    public void setTextAnswers(List<TextAnswerDTO> textAnswers) {
        this.textAnswers = textAnswers;
    }
}

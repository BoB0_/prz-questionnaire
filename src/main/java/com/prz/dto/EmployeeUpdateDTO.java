package com.prz.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeUpdateDTO {

    private Long id;

    @NotEmpty
    private String login;

    private String email;

    private String confirmationEmail;

    private String newPassword;

    private String confirmationNewPassword;

    @NotEmpty
    private String password;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    private Long departmentId;

    private Long cathedralId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmationEmail() {
        return confirmationEmail;
    }

    public void setConfirmationEmail(String confirmationEmail) {
        this.confirmationEmail = confirmationEmail;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmationNewPassword() {
        return confirmationNewPassword;
    }

    public void setConfirmationNewPassword(String confirmationNewPassword) {
        this.confirmationNewPassword = confirmationNewPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getCathedralId() {
        return cathedralId;
    }

    public void setCathedralId(Long cathedralId) {
        this.cathedralId = cathedralId;
    }
}

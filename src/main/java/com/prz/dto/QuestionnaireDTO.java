package com.prz.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.prz.model.Cathedral;
import com.prz.model.Department;
import org.hibernate.validator.constraints.NotEmpty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuestionnaireDTO {

    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    private String creator;

    private List<Department> departments;

    private List<Cathedral> cathedrals;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm ")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime creationDate;

    @NotEmpty
    private List<QuestionDTO> questions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public List<Cathedral> getCathedrals() {
        return cathedrals;
    }

    public void setCathedrals(List<Cathedral> cathedrals) {
        this.cathedrals = cathedrals;
    }
}

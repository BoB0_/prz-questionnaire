package com.prz.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by rbobko on 20.12.2016.
 */
public class SolvedQuestionnaireViewDTO {

    private Long id;

    private EmployeeViewDTO employee;

    private QuestionnaireDTO questionnaire;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm ")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime solvingDate;

    @NotEmpty
    private List<AnswerDTO> answers;

    private List<TextAnswerDTO> textAnswers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmployeeViewDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeViewDTO employee) {
        this.employee = employee;
    }

    public QuestionnaireDTO getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireDTO questionnaire) {
        this.questionnaire = questionnaire;
    }

    public LocalDateTime getSolvingDate() {
        return solvingDate;
    }

    public void setSolvingDate(LocalDateTime solvingDate) {
        this.solvingDate = solvingDate;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }

    public List<TextAnswerDTO> getTextAnswers() {
        return textAnswers;
    }

    public void setTextAnswers(List<TextAnswerDTO> textAnswers) {
        this.textAnswers = textAnswers;
    }
}

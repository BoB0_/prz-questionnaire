package com.prz.dto;

/**
 * Created by rbobko on 20.12.2016.
 */
public class TextAnswerDTO {

    private Long id;

    private Long questionId;

    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

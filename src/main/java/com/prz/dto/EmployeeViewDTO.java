package com.prz.dto;

/**
 * Created by rbobko on 28.12.2016.
 */
public class EmployeeViewDTO {

    private Long id;

    private String login;

    private String email;

    private String firstName;

    private String lastName;

    private Long departmentId;

    private Long cathedralId;

    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getCathedralId() {
        return cathedralId;
    }

    public void setCathedralId(Long cathedralId) {
        this.cathedralId = cathedralId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

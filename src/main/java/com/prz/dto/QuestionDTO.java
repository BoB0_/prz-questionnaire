package com.prz.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.prz.model.QuestionType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuestionDTO {

    private Long id;

    @NotEmpty
    private String text;

    @NotNull
    private QuestionType type;

    @NotEmpty
    private List<AnswerDTO> answers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }
}

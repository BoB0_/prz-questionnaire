package com.prz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;

/**
 * Created by rbobko on 29.12.2016.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({EntityNotFoundException.class, UsernameNotFoundException.class})
    public ResponseEntity<ExceptionDTO> exceptionNotFound(Exception ex){
        ExceptionDTO exceptionDTO = new ExceptionDTO(ex.getMessage(), 404);
        return new ResponseEntity<>(exceptionDTO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({IllegalArgumentException.class, IllegalStateException.class})
    public ResponseEntity<ExceptionDTO> exceptionBadRequest(Exception ex){
        ExceptionDTO exceptionDTO = new ExceptionDTO(ex.getMessage(), 400);
        return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ExceptionDTO> exceptionForbidden(Exception ex){
        ExceptionDTO exceptionDTO = new ExceptionDTO(ex.getMessage(), 403);
        return new ResponseEntity<>(exceptionDTO, HttpStatus.FORBIDDEN);
    }
}

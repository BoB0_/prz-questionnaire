package com.prz.exception;

/**
 * Created by rbobko on 29.12.2016.
 */
public class ExceptionDTO {

    private String message;
    private int code;

    public ExceptionDTO(String message, int code){
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

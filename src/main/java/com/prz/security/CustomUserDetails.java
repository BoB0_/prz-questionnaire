package com.prz.security;

import com.prz.model.Employee;
import com.prz.model.Role;
import com.prz.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbobko on 28.12.2016.
 */
@Service
public class CustomUserDetails implements UserDetailsService {
    private final EmployeeService employeeService;

    @Autowired
    public CustomUserDetails(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }


    @Override
    public UserDetails loadUserByUsername(String username){
        try{
            Employee employee = employeeService.findByLogin(username);

            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            if(employee.getRole().equals(Role.ADMIN)){
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            }
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));

            return new User(employee.getLogin(), employee.getPassword(), grantedAuthorities);
        }catch (Exception e){
            throw new UsernameNotFoundException("User not found");
        }
    }
}

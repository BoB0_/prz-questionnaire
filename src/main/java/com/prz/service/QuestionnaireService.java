package com.prz.service;

import com.prz.dao.QuestionnaireRepository;
import com.prz.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@Service
@Transactional
public class QuestionnaireService {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private SolvedQuestionnaireService solvedQuestionnaireService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private EmployeeService employeeService;

    private Questionnaire save(Questionnaire questionnaire) {
        return questionnaireRepository.save(questionnaire);
    }

    public Long create(Questionnaire questionnaire, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        questionnaire.setCreatorId(loggedUser.getId());
        Long savedQuestionnaireId = save(questionnaire).getId();
        taskService.createTasks(questionnaire);

        return savedQuestionnaireId;
    }

    public List<Questionnaire> getAll() {
        return questionnaireRepository.findAll();
    }

    public Questionnaire findOne(Long id) {
        Questionnaire questionnaire = questionnaireRepository.findOne(id);
        if (questionnaire != null) {
            return questionnaire;
        } else {
            throw new EntityNotFoundException("Questionnaire with id " + id + " not found");
        }
    }

    public Questionnaire getOne(Long id, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Questionnaire questionnaire = findOne(id);

        if (questionnaire.getCreatorId().equals(loggedUser.getId()) || loggedUser.getRole().equals(Role.ADMIN)) {
            return questionnaire;
        } else {
            throw new AccessDeniedException("You cannot view this questionnaire");
        }
    }

    public void remove(Long id, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Questionnaire questionnaire = findOne(id);

        if (questionnaire.getCreatorId().equals(loggedUser.getId()) || loggedUser.getRole().equals(Role.ADMIN)) {
            questionnaireRepository.delete(id);
            taskService.removeTasks(id);
        } else {
            throw new AccessDeniedException("You cannot remove this questionnaire");
        }
    }

    public Statistics getStatistics(Long questionnaireId, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Questionnaire questionnaire = findOne(questionnaireId);

        if (questionnaire.getCreatorId().equals(loggedUser.getId()) || loggedUser.getRole().equals(Role.ADMIN)) {
            Statistics statistics = new Statistics();
            List<SolvedQuestionnaire> solvedQuestionnaires = solvedQuestionnaireService.findByQuestionnaireId(questionnaireId);

            statistics.setCreationDate(questionnaire.getCreationDate());
            statistics.setCreatorId(questionnaire.getCreatorId());
            statistics.setDescription(questionnaire.getDescription());
            statistics.setName(questionnaire.getName());
            statistics.setQuestionnaireId(questionnaire.getId());
            statistics.setQuestionsStats(createQuestionStats(questionnaire));

            solvedQuestionnaires.forEach(solvedQuestionnaire -> updateStatistics(solvedQuestionnaire, statistics));

            return statistics;
        } else {
            throw new AccessDeniedException("You cannot see statistics for this questionnaire");
        }
    }

    private List<QuestionStats> createQuestionStats(Questionnaire questionnaire) {
        List<QuestionStats> questionStatses = new ArrayList<>();

        questionnaire.getQuestions().forEach(question -> {
            QuestionStats questionStats = new QuestionStats();

            questionStats.setQuestionId(question.getId());
            questionStats.setQuestionText(question.getText());
            questionStats.setQuestionType(question.getType());
            questionStats.setAnswersStats(createAnswerStats(question));
            questionStats.setTextAnswers(new ArrayList<>());

            questionStatses.add(questionStats);
        });

        return questionStatses;
    }

    private List<AnswerStats> createAnswerStats(Question question) {
        List<AnswerStats> answerStatses = new ArrayList<>();

        question.getAnswers().forEach(answer -> {
            AnswerStats answerStats = new AnswerStats();

            answerStats.setAnswerId(answer.getId());
            answerStats.setAnswerText(answer.getText());

            answerStatses.add(answerStats);
        });

        return answerStatses;
    }

    private void updateStatistics(SolvedQuestionnaire solvedQuestionnaire, Statistics statistics) {
        solvedQuestionnaire.getAnswers().forEach(answer -> statistics.getQuestionsStats().stream()
                .filter(question -> question.getQuestionId().equals(answer.getQuestion().getId())).findFirst().get()
                .getAnswersStats().stream().filter(answerStats -> answerStats.getAnswerId().equals(answer.getId())).findFirst().get().incCount());

        solvedQuestionnaire.getTextAnswers().forEach(textAnswer -> {
            statistics.getQuestionsStats().stream()
                    .filter(questionStats -> questionStats.getQuestionId().equals(textAnswer.getQuestion().getId())).findFirst().get().addTextAnswer(textAnswer);
        });
    }

    public void solveQuestionnaire(SolvedQuestionnaire solvedQuestionnaire, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Questionnaire questionnaire = findOne(solvedQuestionnaire.getQuestionnaireId());

        if (solvedQuestionnaire.getEmployeeId().equals(loggedUser.getId())) {
            if (checkIfEmployeeCanSolveQuestionnaire(loggedUser, questionnaire)) {
                solvedQuestionnaireService.create(solvedQuestionnaire);
            } else {
                throw new AccessDeniedException("You cannot solve this questionnaaire");
            }
        } else {
            throw new IllegalStateException("Logged user differs from solved questionnaire user");
        }
    }

    private boolean checkIfEmployeeCanSolveQuestionnaire(Employee loggedUser, Questionnaire questionnaire) {
        if (questionnaire.getCathedrals().stream().anyMatch(cathedral -> cathedral.getId().equals(loggedUser.getCathedralId()))) {
            return true;
        } else if (questionnaire.getDepartments().stream().anyMatch(department -> department.getId().equals(loggedUser.getDepartmentId()))) {
            return true;
        }
        return false;
    }

    public SolvedQuestionnaire getSolvedQuestionnaire(Long questionnaireId, Long resultId, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Questionnaire questionnaire = findOne(questionnaireId);
        if (questionnaire.getCreatorId().equals(loggedUser.getId()) || loggedUser.getRole().equals(Role.ADMIN)) {
            SolvedQuestionnaire solvedQuestionnaire = solvedQuestionnaireService.findOne(resultId);

            if (solvedQuestionnaire.getQuestionnaireId().equals(questionnaire.getId())) {
                return solvedQuestionnaire;
            } else {
                throw new IllegalArgumentException("Ids not equal");
            }
        } else {
            throw new AccessDeniedException("You cannot see this result");
        }
    }

    public List<Long> getQuestionnaireResultList(Long questionnaireId, Principal principal) {
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Questionnaire questionnaire = findOne(questionnaireId);

        if (questionnaire.getCreatorId().equals(loggedUser.getId()) || loggedUser.getRole().equals(Role.ADMIN)) {
            List<Long> ids = new ArrayList<>();

            List<SolvedQuestionnaire> solvedQuestionnaires = solvedQuestionnaireService.findByQuestionnaireId(questionnaireId);
            solvedQuestionnaires.forEach(solvedQuestionnaire -> ids.add(solvedQuestionnaire.getId()));

            return ids;
        } else {
            throw new AccessDeniedException("You cannot see result list of this questionnaire");
        }
    }

    public long count() {
        return questionnaireRepository.count();
    }
}

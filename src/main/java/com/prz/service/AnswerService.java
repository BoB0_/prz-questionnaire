package com.prz.service;

import com.prz.dao.AnswerRepository;
import com.prz.model.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@Service
@Transactional
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    public Answer findOne(Long id){
        Answer answer = answerRepository.findOne(id);
        if(answer != null){
            return answer;
        }else {
            throw new EntityNotFoundException("Answer with id " + id + " not found");
        }
    }
}

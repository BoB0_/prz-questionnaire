package com.prz.service;

import com.prz.dao.TaskRepository;
import com.prz.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
@Service
@Transactional
public class TaskService {

    private final TaskRepository taskRepository;

    private final EmployeeService employeeService;

    @Autowired
    public TaskService(TaskRepository taskRepository, EmployeeService employeeService) {
        this.taskRepository = taskRepository;
        this.employeeService = employeeService;
    }

    public Task save(Task task){
        return taskRepository.save(task);
    }

    public void remove(Long taskId){
        taskRepository.delete(taskId);
    }

    public Task findOne(Long taskId){
        Task task = taskRepository.findOne(taskId);
        if (task != null){
            return task;
        }else {
            throw new EntityNotFoundException("Task with id " + taskId + " not found");
        }
    }

    public List<Task> findNotDoneEmployeeTasks(Principal principal){
        Employee loggedUser = employeeService.findByLogin(principal.getName());
        Long employeeId = loggedUser.getId();

        return taskRepository.findByEmployeeIdAndDone(employeeId, false);
    }

    public Task findByEmployeeIdAndQuestionnaireId(Long employeeId, Long questionnaireId){

        Task task = taskRepository.findByEmployeeIdAndQuestionnaireId(employeeId, questionnaireId);
        if(task != null){
            return task;
        }else {
            return null;
//            throw new EntityNotFoundException("Task with questionnaire id " + questionnaireId + " for employee with id " + employeeId + " not found");
        }
    }

    public void markAsDone(Long questionnaireId){
        Employee loggedUser = employeeService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        Long employeeId = loggedUser.getId();

        Task task = findByEmployeeIdAndQuestionnaireId(employeeId, questionnaireId);
        if (task != null) {
            task.setDone(true);

            save(task);
        } else {
            throw new EntityNotFoundException("Task with questionnaire id " + questionnaireId + " for employee with id " + employeeId + " not found");
        }
    }

    public void createTasks(Questionnaire questionnaire){
        List<Department> departments = questionnaire.getDepartments();
        List<Cathedral> cathedrals = questionnaire.getCathedrals();

        if(departments == null){
            if(cathedrals == null){
                createTasksForAllEmployees(questionnaire);
            }else {
                cathedrals.forEach(cathedral -> createTasksForCathedralEmployees(cathedral.getId(), questionnaire));
            }
        }else{
            if(cathedrals == null){
                departments.forEach(department -> createTasksForDepartmentEmployees(department.getId(), questionnaire));
            }else {
                for(Cathedral c : cathedrals){
                    if(departments.stream().anyMatch(department -> department.getId().equals(c.getDepartmentId()))){
                        cathedrals.remove(c);
                    }
                }

                departments.forEach(department -> createTasksForDepartmentEmployees(department.getId(), questionnaire));
                cathedrals.forEach(cathedral -> createTasksForCathedralEmployees(cathedral.getId(), questionnaire));
            }
        }
    }

    private void createTasksForDepartmentEmployees(Long departmentId, Questionnaire questionnaire){
        List<Employee> employees = employeeService.findByDepartmentId(departmentId);

        employees.forEach(employee -> createTask(employee.getId(), questionnaire));
    }

    private void createTasksForCathedralEmployees(Long cathedralId, Questionnaire questionnaire){
        List<Employee> employees = employeeService.findByCathedralId(cathedralId);

        employees.forEach(employee -> createTask(employee.getId(), questionnaire));
    }

    private void createTasksForAllEmployees(Questionnaire questionnaire){
        List<Employee> employees = employeeService.findAll();

        employees.forEach(employee -> createTask(employee.getId(), questionnaire));
    }

    private void createTask(Long employeeId, Questionnaire questionnaire){
        Task task = new Task();
        task.setDone(false);
        task.setEmployeeId(employeeId);
        task.setQuestionnaireId(questionnaire.getId());
        task.setQuestionnaireName(questionnaire.getName());

        if (findByEmployeeIdAndQuestionnaireId(employeeId, questionnaire.getId()) == null)
            save(task);
    }

    public void removeTasks(Long questionnaireId){
        List<Task> tasks = taskRepository.findByQuestionnaireId(questionnaireId);

        tasks.forEach(task -> remove(task.getId()));
    }
}

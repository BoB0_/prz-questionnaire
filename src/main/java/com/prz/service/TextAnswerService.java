package com.prz.service;

import com.prz.dao.TextAnswerRepository;
import com.prz.model.Question;
import com.prz.model.TextAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Super Rafał on 12.12.2016.
 */
@Service
@Transactional
public class TextAnswerService {

    @Autowired
    private TextAnswerRepository textAnswerRepository;

    public List<TextAnswer> findByQuestion(Question question){
        return textAnswerRepository.findByQuestion(question);
    }
}

package com.prz.service;

import com.prz.dao.QuestionRepository;
import com.prz.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@Service
@Transactional
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    public Question findOne(Long id){
        Question question = questionRepository.findOne(id);
        if(question != null){
            return question;
        }else {
            throw new EntityNotFoundException("Question with id " + id + " not found");
        }
    }
}

package com.prz.service;

import com.prz.dao.DepartmentRepository;
import com.prz.model.Cathedral;
import com.prz.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private CathedralService cathedralService;

    public List<Department> findAll(){
        return departmentRepository.findAll();
    }

    public Department save(Department department){
        return departmentRepository.save(department);
    }

    public Department create(String name){
        if(findByName(name)==null){
            Department department = new Department();
            department.setName(name);
            return save(department);
        }else {
            throw new IllegalArgumentException("Given name is already taken");
        }
    }

    public Department findByName(String name){
        return departmentRepository.findByName(name);
    }

    public Department findOne(Long id){
        Department department = departmentRepository.findOne(id);
        if(department!=null){
            return department;
        }else {
            throw new EntityNotFoundException("Department with id " + id + " not found");
        }
    }

    public void remove(Long id){
        List<Cathedral> cathedrals = cathedralService.findByDepartmentId(id);

        if(cathedrals.isEmpty()){
            departmentRepository.delete(id);
        }else {
            throw new IllegalArgumentException("Cannot remove department with id " + id);
        }
    }

    public Department update(Long departmentId, Department department){
        Department existingDepartment = findOne(departmentId);

        if(existingDepartment.getId().equals(department.getId())){
               existingDepartment.setName(department.getName());
               return save(existingDepartment);
        }else {
            throw new IllegalArgumentException("Ids not equal");
        }
    }

    public long count() {
        return departmentRepository.count();
    }
}

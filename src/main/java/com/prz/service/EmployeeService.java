package com.prz.service;

import com.prz.dao.EmployeeRepository;
import com.prz.dto.EmployeeCreateDTO;
import com.prz.dto.EmployeeUpdateDTO;
import com.prz.mapper.EmployeeMapper;
import com.prz.model.Cathedral;
import com.prz.model.Employee;
import com.prz.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private CathedralService cathedralService;

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }

    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

    public List<Employee> findByDepartmentId(Long departmentId){
        return employeeRepository.findByDepartmentId(departmentId);
    }

    public List<Employee> findByCathedralId(Long cathedralId){
        return employeeRepository.findByCathedralId(cathedralId);
    }

    public Employee findByEmail(String email){
        return employeeRepository.findByEmail(email);
    }

    public Employee findByLogin(String login){
        return employeeRepository.findByLogin(login);
    }

    public Employee findOne(Long id){
        Employee employee = employeeRepository.findOne(id);

        if(employee!=null){
            return employee;
        }else {
            throw new EntityNotFoundException("Employee with id " + id + " not found");
        }
    }

    public Employee getOne(Long id, Principal principal){
        Employee loggedUser = findByLogin(principal.getName());

        if(loggedUser.getId().equals(id) || loggedUser.getRole().equals(Role.ADMIN)){
            return findOne(id);
        }else {
            throw new AccessDeniedException("You cannot view this user");
        }
    }

    public void register(EmployeeCreateDTO employee){
        validateNewEmployee(employee);
        Employee e = EmployeeMapper.mapToEntity(employee);
        e.setRole(Role.USER);
        save(e);
    }

    private void validateNewEmployee(EmployeeCreateDTO employee){
        if(findByLogin(employee.getLogin())!=null){
            throw new IllegalArgumentException("Given login is already used");
        }
        if(!employee.getEmail().equals(employee.getConfirmedEmail())){
            throw new IllegalArgumentException("Given emails are not equal");
        }
        if(findByEmail(employee.getEmail())!=null){
            throw new IllegalArgumentException("Given email is already used");
        }
        if(!employee.getPassword().equals(employee.getConfirmedPassword())){
            throw new IllegalArgumentException("Given passwords are not equal");
        }
    }

    public void update(Long employeeId, EmployeeUpdateDTO employeeDTO, Principal principal){
        Employee loggedUser = findByLogin(principal.getName());

        if(loggedUser.getId().equals(employeeId) || loggedUser.getRole().equals(Role.ADMIN)) {
            Employee employee = findOne(employeeId);

            if (!employee.getId().equals(employeeDTO.getId())) {
                throw new IllegalArgumentException("Ids not equal");
            } else {
                validateUpdatedEmployee(employee, employeeDTO);
                if (!employeeDTO.getEmail().isEmpty()) {
                    employee.setEmail(employeeDTO.getEmail());
                }
                if (!employeeDTO.getNewPassword().isEmpty()) {
                    employee.setPassword(employeeDTO.getNewPassword());
                }
                employee.setFirstName(employeeDTO.getFirstName());
                employee.setLastName(employeeDTO.getLastName());
                if (employeeDTO.getDepartmentId() != null) {
                    employee.setDepartmentId(employeeDTO.getDepartmentId());
                }
                if (employeeDTO.getCathedralId() != null) {
                    Cathedral cathedral = cathedralService.findOne(employeeDTO.getCathedralId());
                    employee.setDepartmentId(cathedral.getDepartmentId());
                    employee.setCathedralId(cathedral.getId());
                }
                save(employee);
            }
        }else {
            throw new AccessDeniedException("You cannot update this user");
        }
    }

    private void validateUpdatedEmployee(Employee employee, EmployeeUpdateDTO employeeDTO){
        if(!employee.getLogin().equals(employeeDTO.getLogin())){
            throw new IllegalArgumentException("You cannot change login");
        }
        if(!employeeDTO.getEmail().isEmpty()){
            if(!employeeDTO.getEmail().equals(employeeDTO.getConfirmationEmail())){
                throw new IllegalArgumentException("Emails are not equal");
            }
        }
        if(!employeeDTO.getNewPassword().isEmpty()){
            if(!employeeDTO.getNewPassword().equals(employeeDTO.getConfirmationNewPassword())){
                throw new IllegalArgumentException("Passwords are not equal");
            }
        }
        if(employeeDTO.getDepartmentId() != null){
            departmentService.findOne(employeeDTO.getDepartmentId());
        }
        if(employeeDTO.getCathedralId() != null){
            cathedralService.findOne(employeeDTO.getCathedralId());
        }
        if(!employee.getPassword().equals(employeeDTO.getPassword())){
            throw new IllegalArgumentException("Given password is wrong");
        }
    }

    public long count() {
        return employeeRepository.count();
    }
}

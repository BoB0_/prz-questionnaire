package com.prz.service;

import com.prz.dao.CathedralRepository;
import com.prz.model.Cathedral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
@Service
public class CathedralService {

    @Autowired
    private CathedralRepository cathedralRepository;

    public Cathedral save(Cathedral cathedral){
        return cathedralRepository.save(cathedral);
    }

    public List<Cathedral> findAll(){
        return cathedralRepository.findAll();
    }

    public List<Cathedral> findByDepartmentId(Long departmentId){
        return cathedralRepository.findByDepartmentId(departmentId);
    }

    public Cathedral findByName(String name){
        return cathedralRepository.findByName(name);
    }

    public Cathedral create(Cathedral cathedral){
        if(findByName(cathedral.getName())==null){
            return save(cathedral);
        }else {
            throw new IllegalArgumentException("Given cathedral name is already taken");
        }
    }

    public Cathedral findOne(Long id){
        Cathedral cathedral = cathedralRepository.findOne(id);
        if(cathedral!=null){
            return cathedral;
        }else {
            throw new EntityNotFoundException("Cathedral with id " + id + " not found");
        }
    }

    public void remove(Long id){
        Cathedral cathedral = findOne(id);
        cathedralRepository.delete(cathedral);
    }

    public Cathedral update(Long id, Cathedral cathedral){
        Cathedral existingCathedral = findOne(id);

        if(existingCathedral.getId().equals(cathedral.getId())){
            existingCathedral.setName(cathedral.getName());
            existingCathedral.setDepartmentId(cathedral.getDepartmentId());
            return save(existingCathedral);
        }else {
            throw new IllegalArgumentException("Ids not equal");
        }
    }

    public long count() {
        return cathedralRepository.count();
    }
}

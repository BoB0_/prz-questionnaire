package com.prz.service;

import com.prz.dao.SolvedQuestionnaireRepository;
import com.prz.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Super Rafał on 12.12.2016.
 */
@Service
@Transactional
public class SolvedQuestionnaireService {

    @Autowired
    private SolvedQuestionnaireRepository solvedQuestionnaireRepository;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private TaskService taskService;

    public List<SolvedQuestionnaire> findByQuestionnaireId(Long questionnaireId){
        return solvedQuestionnaireRepository.findByQuestionnaireId(questionnaireId);
    }

    public SolvedQuestionnaire create(SolvedQuestionnaire solvedQuestionnaire){
        validate(solvedQuestionnaire);
        SolvedQuestionnaire savedSolvedQuestionnaire =  solvedQuestionnaireRepository.save(solvedQuestionnaire);
        taskService.markAsDone(solvedQuestionnaire.getQuestionnaireId());

        return savedSolvedQuestionnaire;
    }

    public SolvedQuestionnaire findOne(Long id){
        SolvedQuestionnaire solvedQuestionnaire = solvedQuestionnaireRepository.findOne(id);
        if(solvedQuestionnaire != null){
            return solvedQuestionnaire;
        }else {
            throw new EntityNotFoundException("Solved questionnaire with id " + id + " not found");
        }
    }

    public List<SolvedQuestionnaire> getAll(){
        return solvedQuestionnaireRepository.findAll();
    }

    private void validate(SolvedQuestionnaire solvedQuestionnaire){
        Questionnaire questionnaire = questionnaireService.findOne(solvedQuestionnaire.getQuestionnaireId());

        questionnaire.getQuestions().forEach(question -> {
            if(question.getType().equals(QuestionType.SINGLE)){
                validateSingleQuestionAnswers(question.getId(), solvedQuestionnaire);
            }else if(question.getType().equals(QuestionType.MULTIPLE)){
                validateMultipleQuestionAnswers(question.getId(), solvedQuestionnaire);
            }else {
                validateTextQuestionAnswers(question.getId(), solvedQuestionnaire);
            }
        });
    }

    private void validateSingleQuestionAnswers(Long questionId, SolvedQuestionnaire solvedQuestionnaire){
        List<Answer> answers = solvedQuestionnaire.getAnswers().stream()
                .filter(answer -> answer.getQuestion().getId().equals(questionId)).collect(Collectors.toList());

        if(answers.size() == 1){
            return;
        }else {
            throw new IllegalArgumentException("There are multiple answers to question with id " + questionId);
        }
    }

    private void validateMultipleQuestionAnswers(Long questionId, SolvedQuestionnaire solvedQuestionnaire){
        List<Answer> answers = solvedQuestionnaire.getAnswers().stream()
                .filter(answer -> answer.getQuestion().getId().equals(questionId)).collect(Collectors.toList());

        if(answers.size() >= 1){
            return;
        }else {
            throw new IllegalArgumentException("There are no answers to question with id " + questionId);
        }
    }

    private void validateTextQuestionAnswers(Long questionId, SolvedQuestionnaire solvedQuestionnaire){
        List<TextAnswer> answers = solvedQuestionnaire.getTextAnswers().stream()
                .filter(textAnswer -> textAnswer.getQuestion().getId().equals(questionId)).collect(Collectors.toList());

        if(answers.size() == 1){
            return;
        }else {
            throw new IllegalArgumentException("There are multiple answers to question with id " + questionId);
        }
    }
}

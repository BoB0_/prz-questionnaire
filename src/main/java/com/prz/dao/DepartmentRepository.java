package com.prz.dao;

import com.prz.model.Department;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
public interface DepartmentRepository extends CrudRepository<Department, Long> {
    List<Department> findAll();
    Department findByName(String name);
}

package com.prz.dao;

import com.prz.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findAll();
    List<Task> findByEmployeeId(Long employeeId);
    Task findByEmployeeIdAndQuestionnaireId(Long employeeId, Long questionnaireId);
    List<Task> findByEmployeeIdAndDone(Long employeeId, boolean done);
    List<Task> findByQuestionnaireId(Long questionnaireId);
}

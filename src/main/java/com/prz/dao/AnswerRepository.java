package com.prz.dao;

import com.prz.model.Answer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rbobko on 20.12.2016.
 */
public interface AnswerRepository extends CrudRepository<Answer, Long> {
}

package com.prz.dao;

import com.prz.model.Question;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rbobko on 20.12.2016.
 */
public interface QuestionRepository extends CrudRepository<Question, Long> {
}

package com.prz.dao;

import com.prz.model.Cathedral;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
public interface CathedralRepository extends CrudRepository<Cathedral, Long> {
    List<Cathedral> findAll();
    List<Cathedral> findByDepartmentId(Long departmentId);
    Cathedral findByName(String name);
}

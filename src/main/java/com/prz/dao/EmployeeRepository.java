package com.prz.dao;

import com.prz.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by rbobko on 27.12.2016.
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    List<Employee> findAll();
    List<Employee> findByDepartmentId(Long departmentId);
    List<Employee> findByCathedralId(Long cathedralId);
    Employee findByEmail(String email);
    Employee findByLogin(String login);
}

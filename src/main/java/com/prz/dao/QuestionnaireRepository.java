package com.prz.dao;

import com.prz.model.Questionnaire;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
public interface QuestionnaireRepository extends CrudRepository<Questionnaire, Long>{
    List<Questionnaire> findAll();
}

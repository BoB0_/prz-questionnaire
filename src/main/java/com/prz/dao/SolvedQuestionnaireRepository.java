package com.prz.dao;

import com.prz.model.SolvedQuestionnaire;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Super Rafał on 12.12.2016.
 */
public interface SolvedQuestionnaireRepository extends CrudRepository<SolvedQuestionnaire, Long> {
    List<SolvedQuestionnaire> findByQuestionnaireId(Long questionnaireId);
    List<SolvedQuestionnaire> findAll();
}

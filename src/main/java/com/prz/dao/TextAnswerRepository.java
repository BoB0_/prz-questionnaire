package com.prz.dao;

import com.prz.model.Question;
import com.prz.model.TextAnswer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Super Rafał on 12.12.2016.
 */
public interface TextAnswerRepository extends CrudRepository<TextAnswer, Long> {
    List<TextAnswer> findByQuestion(Question question);
}

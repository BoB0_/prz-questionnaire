package com.prz.controller;

import com.prz.model.DashboardInfo;
import com.prz.service.CathedralService;
import com.prz.service.DepartmentService;
import com.prz.service.EmployeeService;
import com.prz.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Wojciech on 2017-01-11.
 */
@RestController
public class DashboardController {
    private final QuestionnaireService questionnaireService;
    private final EmployeeService employeeService;
    private final DepartmentService departmentService;
    private final CathedralService cathedralService;

    @Autowired
    public DashboardController(QuestionnaireService questionnaireService, EmployeeService employeeService, DepartmentService departmentService, CathedralService cathedralService) {
        this.questionnaireService = questionnaireService;
        this.employeeService = employeeService;
        this.departmentService = departmentService;
        this.cathedralService = cathedralService;
    }

    @GetMapping("/api/dashboard")
    public ResponseEntity<DashboardInfo> getDashboardInfo() {
        DashboardInfo dashboardInfo = new DashboardInfo();
        dashboardInfo.setQuestionnairesCount(questionnaireService.count());
        dashboardInfo.setEmployeesCount(employeeService.count());
        dashboardInfo.setDepartmentsCount(departmentService.count());
        dashboardInfo.setCathedralsCount(cathedralService.count());
        return ResponseEntity.ok(dashboardInfo);
    }
}

package com.prz.controller;

import com.prz.dto.EmployeeCreateDTO;
import com.prz.dto.EmployeeUpdateDTO;
import com.prz.mapper.EmployeeMapper;
import com.prz.model.Employee;
import com.prz.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rbobko on 27.12.2016.
 */
@RestController
@RequestMapping(value = "/api/employees")
@PreAuthorize("hasRole('ROLE_USER')")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PreAuthorize("isAnonymous()")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody @Valid EmployeeCreateDTO employee){
        employeeService.register(employee);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getOne(@PathVariable Long id, Principal principal){
        return ResponseEntity.ok(EmployeeMapper.loadFromEntity(employeeService.getOne(id, principal)));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable Long id, @RequestBody @Valid EmployeeUpdateDTO employeeDTO, Principal principal){
        employeeService.update(id, employeeDTO, principal);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/currentUser", method = RequestMethod.GET)
    public ResponseEntity getCurrentUser(Principal user) {
        Employee e = employeeService.findByLogin(user.getName());
        if (e == null)
            return ResponseEntity.notFound().build();
        Map<String, String> userMap = new HashMap<>();
        userMap.put("id", e.getId().toString());
        userMap.put("username", e.getLogin());
        userMap.put("firstName", e.getFirstName());
        userMap.put("lastName", e.getLastName());
        userMap.put("emailAddress", e.getEmail());
        userMap.put("role", e.getRole().toString());
        if(e.getDepartmentId() != null) {
            userMap.put("departmentId", e.getDepartmentId().toString());
        }
        if(e.getCathedralId() != null) {
            userMap.put("cathedralId", e.getCathedralId().toString());
        }

        return ResponseEntity.ok(userMap);
    }
}

package com.prz.controller;

import com.prz.dto.EmployeeCreateDTO;
import com.prz.model.Employee;
import com.prz.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

/**
 * Created by Wojciech on 2017-01-02.
 */
@Controller
public class HomeController {
    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView getLoginPage(@RequestParam Optional<String> error) {
        return new ModelAndView("login", "error", error);
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView getRegisterPage(Model model) {
        EmployeeCreateDTO employee = new EmployeeCreateDTO();
        model.addAttribute("employee", employee);
        return new ModelAndView("register", "model", model);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView getRegister(@ModelAttribute("employee") EmployeeCreateDTO employee) {
        if (valid(employee)) {
//            EmployeeCreateDTO employee = new EmployeeCreateDTO();
//            employee.setLogin(model.asMap().get("login").toString());
//            employee.setEmail(model.asMap().get("email").toString());
//            employee.setConfirmedEmail(model.asMap().get("confirmedEmail").toString());
//            employee.setPassword(model.asMap().get("password").toString());
//            employee.setConfirmedPassword(model.asMap().get("confirmedPassword").toString());
//            employee.setFirstName(model.asMap().get("firstName").toString());
//            employee.setLastName(model.asMap().get("lastName").toString());
            employeeService.register(employee);
            return new ModelAndView("redirect:/login");
        }
        return new ModelAndView("register");
    }

    private boolean valid(EmployeeCreateDTO employee) {
        return employee.getLogin() != null &&
                employee.getEmail() != null &&
                employee.getConfirmedEmail() != null &&
                employee.getFirstName() != null &&
                employee.getLastName() != null &&
                employee.getPassword() != null &&
                employee.getConfirmedPassword() != null;
    }
}

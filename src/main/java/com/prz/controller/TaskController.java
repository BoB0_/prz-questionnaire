package com.prz.controller;

import com.prz.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by rbobko on 27.12.2016.
 */
@RestController
@RequestMapping(value = "/api/tasks")
@PreAuthorize("hasRole('ROLE_USER')")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getNotDoneEmployeeTasks(Principal principal){
        return ResponseEntity.ok(taskService.findNotDoneEmployeeTasks(principal));
    }
}

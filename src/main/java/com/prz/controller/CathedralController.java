package com.prz.controller;

import com.prz.model.Cathedral;
import com.prz.service.CathedralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by rbobko on 27.12.2016.
 */
@RestController
@RequestMapping(value = "/api/cathedrals")
@PreAuthorize("hasRole('ROLE_USER')")
public class CathedralController {

    @Autowired
    private CathedralService cathedralService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAll(){
        return ResponseEntity.ok(cathedralService.findAll());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody @Valid Cathedral cathedral){
        cathedralService.create(cathedral);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getOne(@PathVariable Long id){
        return ResponseEntity.ok(cathedralService.findOne(id));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable Long id){
        cathedralService.remove(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable Long id, Cathedral cathedral){
        cathedralService.update(id, cathedral);
        return ResponseEntity.noContent().build();
    }
}

package com.prz.controller;

import com.prz.dto.QuestionnaireDTO;
import com.prz.dto.SolvedQuestionnaireCreateDTO;
import com.prz.mapper.QuestionnaireMapper;
import com.prz.mapper.SolvedQuestionnaireMapper;
import com.prz.model.Statistics;
import com.prz.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Super Rafał on 11.12.2016.
 */
@RestController
@RequestMapping(value = "/api/questionnaires")
@PreAuthorize("hasRole('ROLE_USER')")
public class QuestionnaireController {

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private SolvedQuestionnaireMapper solvedQuestionnaireMapper;

    @Autowired
    private QuestionnaireMapper questionnaireMapper;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAll() {
        return ResponseEntity.ok(questionnaireMapper.loadFromEntity(questionnaireService.getAll()));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getOne(@PathVariable Long id, Principal principal) {
        return ResponseEntity.ok(questionnaireMapper.loadFromEntity(questionnaireService.getOne(id, principal)));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createQuestionnaire(@RequestBody @Valid QuestionnaireDTO questionnaire, Principal principal) {
        questionnaireService.create(questionnaireMapper.mapToEntity(questionnaire), principal);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeQuestionnaire(@PathVariable Long id, Principal principal) {
        questionnaireService.remove(id, principal);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/statistics", method = RequestMethod.GET)
    public ResponseEntity getQuestionnaireStatistics(@PathVariable Long id, Principal principal) {
        Statistics statistics = questionnaireService.getStatistics(id, principal);
        return ResponseEntity.ok(statistics);
    }

    @RequestMapping(value = "{id}/results", method = RequestMethod.POST)
    public ResponseEntity solveQuestionnaire(@RequestBody SolvedQuestionnaireCreateDTO solvedQuestionnaire, Principal principal){
        questionnaireService.solveQuestionnaire(solvedQuestionnaireMapper.mapToEntity(solvedQuestionnaire), principal);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{questionnaireId}/results/{resultId}", method = RequestMethod.GET)
    public ResponseEntity getSolvedQuestionnaire(@PathVariable Long questionnaireId, @PathVariable Long resultId, Principal principal){
        return ResponseEntity.ok(solvedQuestionnaireMapper.loadFromEntity(questionnaireService.getSolvedQuestionnaire(questionnaireId, resultId, principal)));
    }

    @RequestMapping(value = "/{questionnaireId}/results", method = RequestMethod.GET)
    public ResponseEntity getQuestionnaireResultList(@PathVariable Long questionnaireId, Principal principal){
        List<Long> ids = questionnaireService.getQuestionnaireResultList(questionnaireId, principal);
        return ResponseEntity.ok(ids);
    }
}

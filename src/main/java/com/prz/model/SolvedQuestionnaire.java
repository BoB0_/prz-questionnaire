package com.prz.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Super Rafał on 07.12.2016.
 */
@Entity
public class SolvedQuestionnaire {

    @Id
    @GeneratedValue
    private Long id;

    private Long employeeId;

    private Long questionnaireId;

    private LocalDateTime solvingDate;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<Answer> answers;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<TextAnswer> textAnswers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public LocalDateTime getSolvingDate() {
        return solvingDate;
    }

    public void setSolvingDate(LocalDateTime solvingDate) {
        this.solvingDate = solvingDate;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public List<TextAnswer> getTextAnswers() {
        return textAnswers;
    }

    public void setTextAnswers(List<TextAnswer> textAnswers) {
        this.textAnswers = textAnswers;
    }
}

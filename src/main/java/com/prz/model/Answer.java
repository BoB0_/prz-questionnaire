package com.prz.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;

/**
 * Created by Super Rafał on 07.12.2016.
 */
@Entity
public class Answer {

    @Id
    @GeneratedValue
    private Long id;

    private String text;

    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Question question;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}

package com.prz.model;

/**
 * Created by Wojciech on 2017-01-11.
 */
public class DashboardInfo {
    private long questionnairesCount;
    private long employeesCount;
    private long departmentsCount;
    private long cathedralsCount;

    public long getQuestionnairesCount() {
        return questionnairesCount;
    }

    public void setQuestionnairesCount(long questionnairesCount) {
        this.questionnairesCount = questionnairesCount;
    }

    public long getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(long employeesCount) {
        this.employeesCount = employeesCount;
    }

    public long getDepartmentsCount() {
        return departmentsCount;
    }

    public void setDepartmentsCount(long departmentsCount) {
        this.departmentsCount = departmentsCount;
    }

    public long getCathedralsCount() {
        return cathedralsCount;
    }

    public void setCathedralsCount(long cathedralsCount) {
        this.cathedralsCount = cathedralsCount;
    }
}

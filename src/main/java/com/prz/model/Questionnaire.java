package com.prz.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Super Rafał on 07.12.2016.
 */
@Entity
public class Questionnaire {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private Long creatorId;

    private LocalDateTime creationDate;

    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "questionnaire_department", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "id"))
    private List<Department> departments;

    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "questionnaire_cathedral", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "id"))
    private List<Cathedral> cathedrals;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Question> questions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public List<Cathedral> getCathedrals() {
        return cathedrals;
    }

    public void setCathedrals(List<Cathedral> cathedrals) {
        this.cathedrals = cathedrals;
    }
}

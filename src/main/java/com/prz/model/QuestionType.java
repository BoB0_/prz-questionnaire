package com.prz.model;

/**
 * Created by Super Rafał on 07.12.2016.
 */
public enum QuestionType {
    SINGLE("SINGLE"),
    MULTIPLE("MULTIPLE"),
    TEXT("TEXT");

    private final String text;

    QuestionType(final String text){
        this.text = text;
    }

    public String toString(){
        return this.text;
    }
}

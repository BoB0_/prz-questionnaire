package com.prz.model;

/**
 * Created by rbobko on 28.12.2016.
 */
public enum Role {
    USER("USER"),
    ADMIN("ADMIN");

    private final String text;

    Role(final String text){
        this.text = text;
    }

    public String toString(){
        return this.text;
    }
}

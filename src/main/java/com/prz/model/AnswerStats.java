package com.prz.model;

/**
 * Created by Super Rafał on 12.12.2016.
 */
public class AnswerStats {

    private Long answerId;

    private String answerText;

    private int count;

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void incCount(){
        this.count = this.count+1;
    }
}

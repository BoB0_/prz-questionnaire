package com.prz.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 12.12.2016.
 */
public class QuestionStats {

    private Long questionId;

    private String questionText;

    private QuestionType questionType;

    private List<AnswerStats> answersStats;

    private List<TextAnswer> textAnswers;

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<AnswerStats> getAnswersStats() {
        return answersStats;
    }

    public void setAnswersStats(List<AnswerStats> answersStats) {
        this.answersStats = answersStats;
    }

    public List<TextAnswer> getTextAnswers() {
        return textAnswers;
    }

    public void setTextAnswers(List<TextAnswer> textAnswers) {
        this.textAnswers = textAnswers;
    }

    public void addTextAnswer(TextAnswer textAnswer){
        if(this.textAnswers == null){
            this.textAnswers = new ArrayList<>();
        }
        this.textAnswers.add(textAnswer);
    }
}

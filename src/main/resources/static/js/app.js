var appModule = angular.module('app',['ngRoute', 'checklist-model']);

appModule.config(function($routeProvider, $httpProvider){
    $routeProvider
        .when('/', {
            templateUrl : 'views/dashboard.html',
            controller : 'homeController'
        })
        .when("/departments", {
            templateUrl : 'views/department-section.html',
            controller : 'departmentController'
        })
        .when("/cathedrals", {
            templateUrl : 'views/cathedral-section.html',
            controller : 'cathedralController'
        })
        .when("/questionnaires", {
            templateUrl : 'views/questionnaire-list.html',
            controller : 'questionnaireController'
        })
        .when("/new-questionnaire", {
            templateUrl : 'views/questionnaire-new.html',
            controller : 'questionnaireNewController'
        })
        .when("/questionnaire/:id", {
            templateUrl : 'views/questionnaire-detail.html',
            controller : 'questionnaireDetailController'
        })
        .when("/questionnaire/solve/:id", {
            templateUrl : 'views/questionnaire-solve.html',
            controller : 'questionnaireSolveController'
        })
        .when("/account", {
            templateUrl : 'views/account-detail.html',
            controller : 'accountController'
        })
        .when("/questionnaire/stats/:id", {
            templateUrl : 'views/questionnaire-stats.html',
            controller : "questionnaireStatsController"
        })
})
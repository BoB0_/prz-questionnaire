angular.module('app').controller('questionnaireSolveController', function ($http, $scope, $routeParams, $location) {
    var id = $routeParams.id;
    if (id) {
        $http.get("/api/questionnaires/"+id)
            .then(function (response) {
                $scope.questionnaire = response.data;
            })
    }
    $scope.solved = {
        questionnaireId : id,
        answersIds : [],
        textAnswers : []
    }

    $scope.submit = function() {
        if (confirm("Czy na pewno chcesz przesłać?")) {
            var questions = $scope.questionnaire.questions;
            for (var i in questions) {

                switch (questions[i].type) {
                    case 'SINGLE':
                        $scope.solved.answersIds.push(questions[i].response);
                        break;
                    case 'MULTIPLE':
                        for (var j in questions[i].responses)
                            $scope.solved.answersIds.push(questions[i].responses[j]);
                        break;
                    case 'TEXT':
                        var textResponse = {
                            questionId : questions[i].id,
                            text : questions[i].textResponse
                        }
                        $scope.solved.textAnswers.push(textResponse);
                        break;
                }
            }
            console.log($scope.solved);
            $http.post("/api/questionnaires/"+id+"/results", $scope.solved)
                .then(function (response) {
                    console.log(response);
                    $location.path("/");
                })
        }
    }
});
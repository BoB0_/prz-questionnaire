angular.module('app').controller('questionnaireNewController', function ($http, $scope, $location) {
    $http.get("/api/departments")
        .then(function(response) {
            $scope.departments = response.data;
        })
    $http.get("/api/cathedrals")
        .then(function(response) {
            $scope.cathedrals = response.data;
        })

    $scope.questionnaire = {
        name : "",
        description : "",
        questions : [],
        departments : [],
        cathedrals : []
    }

    $scope.addQuestion = function() {
        var question = {
            text : "",
            type : "SINGLE",
            answers : []
        }
        $scope.questionnaire.questions.push(question);
    }
    $scope.deleteQuestion = function (question) {
        var index = $scope.questionnaire.questions.indexOf(question);
        $scope.questionnaire.questions.splice(index, 1);
    }
    $scope.addAnswer = function(question) {
        var answer = {
            text : ""
        }
        question.answers.push(answer);
    }
    $scope.deleteAnswer = function (answers, answer) {
        var index = answers.indexOf(answer);
        answers.splice(index, 1);
    }
    $scope.addDepartment = function () {
        var department = {};
        $scope.questionnaire.departments.push(department);
    }
    $scope.deleteDepartment = function (department) {
        var index = $scope.questionnaire.departments.indexOf(department);
        $scope.questionnaire.departments.splice(index, 1);
    }
    $scope.addCathedral = function () {
        var cathedral = {};
        $scope.questionnaire.cathedrals.push(cathedral);
    }
    $scope.deleteCathedral = function (cathedral) {
        var index = $scope.questionnaire.cathedrals.indexOf(cathedral);
        $scope.questionnaire.cathedrals.splice(index, 1);
    }

    $scope.saveQuestionnaire = function () {
        console.log($scope.questionnaire);
        $http.post("/api/questionnaires", $scope.questionnaire)
            .then(function () {
               $location.path("/questionnaires");
            })
    }
});

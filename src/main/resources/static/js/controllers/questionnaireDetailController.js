angular.module('app').controller('questionnaireDetailController', function ($http, $scope, $routeParams) {
    var id = $routeParams.id;
    if (id) {
        $http.get("/api/questionnaires/"+id)
            .then(function (response) {
                $scope.questionnaire = response.data;
            })
    }
});
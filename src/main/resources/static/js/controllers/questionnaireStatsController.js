/**
 * Created by Wojciech on 2017-01-10.
 */
angular.module("app").controller("questionnaireStatsController", function ($http, $scope, $routeParams) {
    var id = $routeParams.id;
    $http.get("/api/questionnaires/"+id+"/statistics")
        .then(function(response) {
            $scope.stats = response.data;
            console.log($scope.stats);
        })
});
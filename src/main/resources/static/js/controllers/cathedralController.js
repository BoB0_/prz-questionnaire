angular.module('app').controller('cathedralController', function ($http, $scope) {
    function initCathedrals() {
        $http.get("/api/cathedrals")
            .then(function (response) {
                $scope.cathedrals = response.data;
                console.log($scope.cathedrals);
            })
    }

    function initDepartments() {
        $http.get("/api/departments")
            .then(function (response) {
                $scope.departments = response.data;
                console.log($scope.departments);
            })
    }

    initDepartments();
    initCathedrals();

    $scope.addCat = function(cat) {
        console.log(cat);
        if (cat) {

            cat.departmentId = cat.department.id;
            cat.department = null;

            $http.post("/api/cathedrals", cat)
                .then(function (response) {
                    console.log(response.status);
                    initCathedrals();
                    $scope.cat = null;
                })
        }
    }
    $scope.deleteCat = function(cat) {
        console.log("delete "+cat.id);
        $http.delete("/api/cathedrals/"+cat.id,"")
            .then(function () {
                var index = $scope.cathedrals.indexOf(cat);
                $scope.cathedrals.splice(index, 1);
            })
    }
})

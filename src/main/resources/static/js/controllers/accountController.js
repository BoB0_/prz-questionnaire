angular.module('app').controller('accountController', function ($http, $scope, $location) {

    $http.get("/api/employees/currentUser")
        .then(function(response) {
            $scope.currentUser = response.data;
        });

    $scope.user = {
        id : $scope.currentUser.id,
        login : $scope.currentUser.username,
        firstName : $scope.currentUser.firstName,
        lastName : $scope.currentUser.lastName,
        departmentId : Number($scope.currentUser.departmentId),
        cathedralId : Number($scope.currentUser.cathedralId),
        email : '',
        confirmationEmail : '',
        newPassword : '',
        confirmationNewPassword : ''
    }
    console.log("user:");
    console.log($scope.user);

    $http.get("/api/departments")
        .then(function (response) {
            $scope.departments = response.data;
            console.log($scope.departments);
        })

    $http.get("/api/cathedrals")
        .then(function (response) {
            $scope.cathedrals = response.data;
            console.log($scope.cathedrals);
        })

    $scope.update = function() {
        console.log($scope.user);
        $http.put("/api/employees/"+$scope.user.id, $scope.user)
            .then(function (response) {
                console.log(response);
            })
    }
});
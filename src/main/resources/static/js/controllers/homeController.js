/**
 * Created by Wojciech on 2017-01-02.
 */
angular.module('app').controller('homeController', function ($http, $scope) {
    console.log("home controller");
    $http.get("/api/tasks")
        .then(function(response) {
            $scope.tasks = response.data;
            console.log(response);
        })

    $http.get("/api/dashboard")
        .then(function(response) {
            console.log(response);
            $scope.dashboard = response.data;
        })

    // for (var i in $scope.tasks) {
    //     $http.get("/api/questionnaires/"+$scope.tasks[i].questionnaireId)
    //         .then(function(response) {
    //             $scope.tasks[i].questionnaireName = response.data.name;
    //         })
    // }
    // $http.get("/api/questionnaires")
    //     .then(function(response) {
    //         $scope.questionnaires = response.data;
    //         console.log($scope.questionnaires);
    //     })
})

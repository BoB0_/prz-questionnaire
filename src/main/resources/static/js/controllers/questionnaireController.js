angular.module('app').controller('questionnaireController', function ($http, $scope) {
    $http.get("/api/questionnaires")
        .then(function (response) {
            $scope.questionnaires = response.data;
        })

    $scope.deleteQuestionnaire = function (q) {
        if (confirm("Czy na pewno chcesz to usunąć? Żeby potem nie było płaczu.")) {
            $http.delete("/api/questionnaires/" + q.id)
                .then(function () {
                    var index = $scope.questionnaires.indexOf(q);
                    $scope.questionnaires.splice(index, 1);
                })
        }
    }
})
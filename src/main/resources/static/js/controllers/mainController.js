angular.module('app').controller('mainController', function ($http, $scope) {
    console.log("main controller");
    $http.get("/api/employees/currentUser")
        .then(function(response) {
            if (!response.data.username)
                window.location = "/login";
            $scope.currentUser = response.data;
            console.log($scope.currentUser);
        });
})
angular.module('app').controller('departmentController', function ($http, $scope, $location) {
    function initDepartments() {
        $http.get("/api/departments")
            .then(function (response) {
                $scope.departments = response.data;
                console.log($scope.departments);
            })
    }

    initDepartments();

    $scope.addDep = function(dep) {
        console.log(dep);
        if (dep) {

            $http.post("/api/departments", dep)
                .then(function (response) {
                    console.log(response.status);
                    initDepartments();
                    $scope.dep = null;
                })
        }
    }
    $scope.deleteDep = function(dep) {
        console.log("delete "+dep.id);
        $http.delete("/api/departments/"+dep.id,"")
            .then(function () {
                var index = $scope.departments.indexOf(dep);
                $scope.departments.splice(index, 1);
            })
    }
})
